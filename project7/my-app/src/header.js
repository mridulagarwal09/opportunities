import React, { Component } from 'react';
import './header.css'
import icon from './images/icon.png'
import Modal from '../node_modules/react-modal'
import cross from './images/cross.svg'
import profileIcon from './images/profileIcon.png'



class Header extends Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }


  closeModal() {
    this.setState({modalIsOpen: false});
  }
  render() {
    return (
      <div className="head">
       <img src={icon} alt="" className="oppLogo"></img>
       <div className="catContainer">
       <p className="category ">Category</p>

       <p className="categoryName">Food and Drinks</p>
     </div>
       <p className="myOpportunities">My Opportunities</p>
       <p className="saved">10 saved</p>
       <p className="slash">|</p>
       <p className="applied"> 2 Applied</p>
       <button type="button" name="button" className="newOpp" onClick={this.openModal}>Post a New Opportunity</button>
       <Modal
         isOpen={this.state.modalIsOpen}
         onAfterOpen={this.afterOpenModal}
         onRequestClose={this.closeModal}
         className="modalCard"
         >
        <div className="modalHeader">
         <p className="profiles">Select Which Profiles to Use</p>
         <img src={cross} onClick={this.closeModal} className="cross"/>

        </div>


       <div className="postOpp">
         <div className="imgContainer">
          <img src={profileIcon} class="profileIcon"/>
          <img src={require('./images/avatar.png')} class="avatar"/>
         </div>

         <div className="nameContainer">
           <h4 className="namePost">Post this Opportunity as Paul Lee</h4>
           <h5 className="indivProfile">Post as Individual profile</h5>
         </div>

       </div>
       <div className="postOpp">
         <div className="imgContainer">
          <img src={require('./images/building.png')} class="avatarBuilding"/>
         </div>

         <div className="nameContainerComp">
           <h4 className="namePost">Post this Opportunity as Company</h4>
           <h5 className="indivProfile">Post as Company profile</h5>
         </div>

       </div>
       </Modal>
      </div>
    );
  }
}

export default Header;
