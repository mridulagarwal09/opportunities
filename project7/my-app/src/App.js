import React, { Component } from 'react';
import Header from './header.js'
import Myapps from './myapps.js'
import SavedOpps from './savedOpp.js'
import PostOpps from './postOpps.js'
import './App.css'


class App extends Component {

  render() {
    return (
      <div className="App">
      <Header/>
      <div className="finalContainer">
      <PostOpps/>
    </div>
      </div>
    );
  }
}

export default App;
