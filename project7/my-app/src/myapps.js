import React, { Component } from 'react';
import './myapps.css'


class Myapps extends Component{
  render(){
    return(
      <div>
        <div className="topCardsContainer">
          <div className="myAppsCard">
<p className="myAppsHeading">My Applications</p>
          </div>

          <div className="savedOpps">
<p className="savedOppsHeading">Saved Opportunities</p>
          </div>
          <div className="postedOpps">
<p className="savedOppsHeading">Posted Opportunities</p>
          </div>
        </div>
        <br/>
        <div className="bottomCardsContainer">
          <div className="leftCardContainer">
<div className="topCardOpp">
<p className="appliedOppsHeading">Applied Opportunities (2)</p>
<hr/>
<ul style={{listStyleType: 'none'}} className="ulli">
  <li className="listCard">
    <div style={{overflow:'auto', paddingBottom:'2%'}}>
    <img src={require('./images/building.png')} className="compAvatar"/>
    <div className="detailsContainerOne">
<p className="oppTitleHead">Opportunity Title</p>
<p className="otherDetails">Company/Personal name</p>
<p className="otherDetails">Location</p>
<p className="otherDetails">2 days ago</p>
    </div>
    <div className="buttonContainer">
<p className="submittedHead">Submitted</p>
<button className="viewButton">View</button>
    </div>
    </div>
    </li>
    <li className="listCard">
      <div style={{overflow:'auto', paddingBottom:'2%'}}>
      <img src={require('./images/building.png')} className="compAvatar"/>
      <div className="detailsContainerOne">
  <p className="oppTitleHead">Opportunity Title</p>
  <p className="otherDetails">Company/Personal name</p>
  <p className="otherDetails">Location</p>
  <p className="otherDetails">2 days ago</p>
      </div>
      <div className="buttonContainer">
  <p className="submittedHead">Submitted</p>
  <button className="viewButton">View</button>
      </div>
      </div>
      </li>

</ul>
</div>


<div className="topCardOpp">
<p className="appliedOppsHeading">Unfinished Applications (2)</p>
<hr/>
<ul style={{listStyleType: 'none'}} className="ulli">
  <li className="listCard">
    <div style={{overflow:'auto', paddingBottom:'2%'}}>
    <img src={require('./images/building.png')} className="compAvatar"/>
    <div className="detailsContainerOne">
<p className="oppTitleHead">Opportunity Title</p>
<p className="otherDetails">Company/Personal name</p>
<p className="otherDetails">Location</p>
<p className="otherDetails">2 days ago</p>
    </div>
    <div className="buttonContainerOne">
<p className="submittedHeadOne">Go back to finish your application</p>
<div style={{overflow:'auto'}}>
<button className="viewButtonOne">View</button>
<button className="editButton">Edit</button>
</div>
    </div>
    </div>
    </li>
    <li className="listCard">
      <div style={{overflow:'auto', paddingBottom:'2%'}}>
      <img src={require('./images/building.png')} className="compAvatar"/>
      <div className="detailsContainerOne">
  <p className="oppTitleHead">Opportunity Title</p>
  <p className="otherDetails">Company/Personal name</p>
  <p className="otherDetails">Location</p>
  <p className="otherDetails">2 days ago</p>
      </div>
      <div className="buttonContainerOne">
  <p className="submittedHeadOne">Go back to finish your application</p>
  <div style={{overflow:'auto'}}>
  <button className="viewButtonOne">View</button>
  <button className="editButton">Edit</button>
  </div>
      </div>
      </div>
      </li>


</ul>
</div>
          </div>
          <div className="rightCardContainer">
            <p className="appliedOppsHeadingOne">Saved Opportunities</p>
            <hr/>
            <ul style={{listStyleType: 'none'}} className="ulli">
<li className="listCardOne">
  <img src={require('./images/building.png')} className="compAvatarOne"/>
  <div className="detailsContainerOne">
<p className="oppTitleHead">Opportunity Title</p>
<p className="otherDetails">Company/Personal name</p>
<p className="otherDetails">Location</p>
<p className="otherDetails">2 days ago</p>
  </div>
</li>
<li className="listCardOne">
  <img src={require('./images/building.png')} className="compAvatarOne"/>
  <div className="detailsContainerOne">
<p className="oppTitleHead">Opportunity Title</p>
<p className="otherDetails">Company/Personal name</p>
<p className="otherDetails">Location</p>
<p className="otherDetails">2 days ago</p>
  </div>
</li>

            </ul>

          </div>
        </div>
      </div>
    );
  }
}


export default Myapps;
